package arvorebp.run;

import arvorebp.services.Loadcsv;
import arvorebp.services.BulkLoader;
import arvorebp.model.Arquivo;
import java.util.ArrayList;

/**
* args[0] arquivo.csv
* args[1] atributo a ser indexado
* args[2] num caracteres indexados
* args[3] ordem da arvore
*/
public class Main
{
	public static void main(String[] args)
	{
		Loadcsv l = new Loadcsv(args[0], args[1]);
		ArrayList<Arquivo> lista =  l.load();
		BulkLoader bl = new BulkLoader(args[1], args[2], args[3]);
		bl.insertInto(lista);
	}
}

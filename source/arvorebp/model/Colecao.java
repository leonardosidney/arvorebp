package arvorebp.model;

import java.util.ArrayList;
import arvorebp.model.Arquivo;

class Colecao 
{
	ArrayList<Arquivo> arquivo;
	public Colecao(){
		arquivo = new ArrayList<Arquivo>();
	}

	public Colecao(ArrayList<Arquivo> arq)
	{
		this.arquivo = arq;
	}
	
	public void add(Arquivo arq)
	{
		this.arquivo.add(arq);
	}

	public void add(ArrayList<Arquivo> arq)
	{
		this.arquivo = arq;
	}

	public void remove(int i)
	{
		this.arquivo.remove(i);
	}

	public Arquivo get(int i)
	{
		return this.arquivo.get(i);
	}
	public ArrayList<Arquivo> get() 
	{
		return this.arquivo;
	}
}
package arvorebp.model;

/**
* Classe que armazena os dados do arquivo .csv.
*
* vetor[0] - id
* vetor[1] - title
* vetor[2] - authors
* vetor[3] - venue
* vetor[4] - year
*/
public class Arquivo
{
	private String [] valor = new String[5];

	public Arquivo(String[] arquivo)
	{
		this.valor[0] = arquivo[0];
		this.valor[1] = arquivo[1];
		this.valor[2] = arquivo[2];
		this.valor[3] = arquivo[3];
		this.valor[4] = arquivo[4];
	}

	public void setValor(String valor, int pos)
	{
		this.valor[pos] = valor;
	}

	public String getValor(int pos)
	{
		return this.valor[pos];
	}
}

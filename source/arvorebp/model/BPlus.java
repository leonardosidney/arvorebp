package arvorebp.model;

import arvorebp.model.Arquivo;
import java.util.ArrayList;
/**
* Estrutura da b+ usada para salvar os nodos
*/
public class BPlus
{
	private Colecao[] arquivo;
	private BPlus[] ponteiros;
	private BPlus pai;
	private boolean eFolha;
	private int ordem;
	private BPlus nodoIrmao; //Proximo a direita
	private String[] index;
	private int indexLength;
	private int ptLength;

	public BPlus(int ordem)
	{
		this.eFolha = false;
		this.ordem = ordem;
		this.ponteiros = new BPlus[this.ordem];
		this.index = new String[ordem];
	}

	public int getOrdem()
	{
		return this.ordem;
	}

	public void setEFolha(boolean decisao)
	{
		this.eFolha = decisao;
		if(decisao) {
			arquivo = new Colecao[ordem-1];
			this.index = new String[ordem-1];
			ponteiros = null;
		} else {
			arquivo = null;
			ponteiros = new BPlus[this.ordem];
		}
	}

	public boolean getEFolha()
	{
		return this.eFolha;
	}

	private int findIndex(String index) {
		for (int i = 0;i < this.length() ; i++) {
			if (index.equals(this.index[i])) {
				return i;
			}
		}
		return -1;
	}


	private void setArray(String index, Colecao colle) {
		this.index[indexLength] = index;
		this.arquivo[indexLength] = colle;
		indexLength++;
	}

	public void setElementoArray(String index, Arquivo arquivo)
	{
		int i = 0;
		if (indexLength > 0) {
			while(!index.equals(this.index[i]) && i < indexLength){
				i++;
			}
			if(index.equals(this.index[i])) {
				this.arquivo[i].add(arquivo);
			} else {
				if (indexLength < ordem-1)
				{
					this.arquivo[indexLength] = new Colecao();
					this.arquivo[indexLength].add(arquivo);
					this.index[indexLength] = index;
					indexLength++;
				} else {
					System.out.println("nodo cheio!");
				}
			}
		} else {
			this.index[0] = index;
			this.arquivo[0] = new Colecao();
			this.arquivo[0].add(arquivo);
			indexLength++;
		}
	}

	public int length()
	{
		if (this.eFolha)
		{
			return indexLength;
		} else {
			return ptLength;
		}
	}

	public void setPai(BPlus pai)
	{
		this.pai = pai;
	}

	public BPlus getPai()
	{
		return this.pai;
	}

	public void setNodoIrmao(BPlus nodoIrmao)
	{
		this.nodoIrmao = nodoIrmao;
	}

	public BPlus getNodoIrmao()
	{
		return this.nodoIrmao;
	}

	public String getIndex(int pos) 
	{
		if (pos < (this.length())) {
			return index[pos];
		}
		return null;
	}

	public int paiLength()
	{
		return ptLength;
	}

	public void split(BPlus novo, int pos)
	{
		int tamanho = this.length();
		while(pos < tamanho) {
			novo.setArray(this.index[pos],this.arquivo[pos]);
			this.arquivo[pos] = null;
			this.index[pos] = null;
			this.indexLength--;
			pos++;
		}
	}

	public void splitPai(BPlus novo, int pos)
	{
		int tamanho = this.length();
		while(pos < tamanho) {
			novo.setPonteiro(this.ponteiros[pos]);
			novo.setIndex(this.index[pos]);
			this.ponteiros[pos] = null;
			this.index[pos] = null;
			this.ptLength--;
			this.indexLength--;
			pos++;
		}
	}

	public void setIndex(String index)
	{
		if (this.eFolha)
		{
			System.out.println("Você não pode colocar um index em um objeto que não é folha sem seu offset");
			return;
		}
		this.index[indexLength] = index;
		indexLength++;
	}


	public void deleteIndex(int index)
	{
		if (this.eFolha){
			this.index[index] = null;
			this.indexLength--;

			while(index < ordem - 1){
				if(index + 1 == ordem - 1 ){
					this.index[index] = null;
					break;
				}
				this.index[index] = this.index[index + 1];
				index++;
			}
		}
	}

	public void setPonteiro(BPlus btree)
	{
		if (this.eFolha)
		{
			System.out.println("Folhas não tem ponteiros para nodos, apenas offsets para arquivos");
			return;
		}
		this.ponteiros[ptLength] = btree;
		this.ptLength++;
	}

	public boolean hasPoint(BPlus btree)
	{
		for (int i = 0; i < this.ptLength; i++) {
			if (btree == this.ponteiros[i]) {
				return true;
			}
		}
		return false;
	}

	public String[] getIndex()
	{
		return index;
	}
	public BPlus getPonteiro(int i)
	{
		if (this.eFolha) {
			System.out.println("folhas não tem ponteiros");
			return null;
		}
		return ponteiros[i];
	}
	public String getIndexEx()
	{
		String temp = index[0];
		for (int i = 1; i < this.length(); i++) {
			index[i-1] = index[i];
			ponteiros[i-1] = ponteiros[i];
		}
		indexLength--;
		return temp;
	}
	public ArrayList<Arquivo> getArquivo(int i) {
		if (i >= 0 && i < (this.length())) {
			return this.arquivo[i].get();
		}
		else {
			return null;
		}
	}
}


package arvorebp.services;

import arvorebp.model.BPlus;
import arvorebp.model.Arquivo;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;

/**
* Classe responsável por fazer a inserção em massa na árvore b+
*/
public class BulkLoader
{
	private int atributo;
	private int numCaracter;
	private int ordem;
	private int ordemMinima;
	private BPlus raiz;
	private BPlus nodo;

	public BulkLoader(String atributo, String numCaracter, String ordem)
	{
		this.atributo = Integer.valueOf(atributo);
		this.numCaracter = Integer.valueOf(numCaracter);
		this.ordem = Integer.valueOf(ordem);
		this.ordemMinima = Math.round((this.ordem)/2);
	}

	public void insertInto(ArrayList<Arquivo> arquivo)
	{
		int i = 0;
		String indice = " ";
		String valorAtual;

		while (i < arquivo.size()) {
			//indices repetidos são tirados
			if((arquivo.get(i).getValor(this.atributo)).length() < numCaracter)
				valorAtual = (arquivo.get(i).getValor(this.atributo)).trim();
			else
				valorAtual = ((arquivo.get(i).getValor(this.atributo)).trim()).substring(0, numCaracter);

			this.nicoNicoNii(arquivo.get(i), valorAtual);
			i++;
		}
		//imprimir a árvore
		this.printaBplus();
		//this.exibeBPlus("conf/A");
		//buscar valor na árvore
		this.menu();
	}

	public void nicoNicoNii(Arquivo arquivo, String index)
	{
		if (this.raiz == null) {
			this.raiz = new BPlus(this.ordem);
			this.raiz.setEFolha(true);
			this.raiz.setElementoArray(index,arquivo);
		} else if (this.raiz.getEFolha()) {

			if (this.raiz.length() < ordem-1) {
				this.raiz.setElementoArray(index,arquivo);
			} else {
				this.raiz = split(raiz, arquivo, index);
			}
		} else {
			if(nodo.length() == ordem-1){
				this.split(nodo,arquivo,index);
			} else {
				nodo.setElementoArray(index,arquivo);
			}
		}
	}
	/**
	*
	* Verificar se o nodo possui pai, se o pai está cheio.
	*/
	public BPlus split(BPlus nodo, Arquivo arquivo, String index)
	{
		if (nodo.getPai() == null) {
			BPlus irmao = new BPlus(ordem);
			BPlus pai = new BPlus(ordem);
			irmao.setEFolha(true);
			nodo.split(irmao,ordemMinima);
			nodo.setNodoIrmao(irmao);
			irmao.setPai(pai);
			nodo.setPai(pai);
			pai.setPonteiro(nodo);
			pai.setPonteiro(irmao);
			pai.setIndex(irmao.getIndex(0));
			irmao.setElementoArray(index,arquivo);
			this.nodo = irmao;

			return pai;
		} else if (nodo.getPai().length() < ordem-1) {
			// O pai do nodo pode receber mais um filho
			BPlus irmao = new BPlus(ordem);
			irmao.setEFolha(true);
			nodo.split(irmao,ordemMinima);
			nodo.setNodoIrmao(irmao);
			irmao.setPai(nodo.getPai());
			irmao.setElementoArray(index,arquivo);
			nodo.getPai().setPonteiro(irmao);
			nodo.getPai().setIndex(irmao.getIndex(0));
			this.nodo = irmao;

			return nodo.getPai();
		} else {
			//Pai está cheio
			BPlus pai = splitPai(nodo.getPai());
			BPlus irmao = new BPlus(this.ordem);
			irmao.setEFolha(true);
			nodo.split(irmao,ordemMinima);
			irmao.setPai(pai);
			nodo.setNodoIrmao(irmao);
			irmao.setElementoArray(index,arquivo);
			pai.setIndex(irmao.getIndex(0));
			this.nodo = irmao;

			return pai;
		}
	}

	/**
	*
	* Verifica o pai do pai está cheio.
	*/
	private BPlus splitPai(BPlus nodo)
	{
		if (nodo.getPai() == null) {
			//nodo raiz
			BPlus irmao = new BPlus(this.ordem);
			BPlus pai = new BPlus(this.ordem);
			this.raiz = pai;
			nodo.splitPai(irmao,ordemMinima);
			nodo.setPai(pai);
			irmao.setPai(pai);
			pai.setPonteiro(nodo);
			pai.setIndex(irmao.getIndex(0));

			return irmao;
		} else if(nodo.getPai().length() < this.ordem-1) {
			//raiz cheia
			BPlus irmao = new BPlus(this.ordem);
			nodo.splitPai(irmao,ordemMinima);
			irmao.setPai(nodo.getPai());
			nodo.getPai().setPonteiro(irmao);
			nodo.getPai().setIndex(irmao.getIndex(0));

			return irmao;
		} else {
			// cria um novo pai e irmao
			BPlus pai = this.splitPai(nodo.getPai());
			BPlus irmao = new BPlus(this.ordem);
			nodo.splitPai(irmao,ordemMinima);
			irmao.setPai(pai);
			pai.setPonteiro(irmao);
			pai.setIndex(irmao.getIndexEx());

			return irmao;
		}
	}

	private void printaBplus()
	{
		int i = 0, j = 0;
		BPlus arvore = this.raiz;

		while(!arvore.getEFolha()){
			while(i < arvore.length()){

				if(arvore.getIndex(i) != null)
					System.out.println("Pai "+ i +"--"+ arvore.getIndex(i));
				i++;
			}
			arvore = arvore.getPonteiro(0);
		}

		while(arvore != null){
			i = 0;

			while(i < ordem - 1){
				System.out.println("Filho "+ j +"--"+arvore.getIndex(i));
				i++;
			}
			j++;
			arvore = arvore.getNodoIrmao();
		}
	}

	private void menu()
	{
		java.util.Scanner a = new java.util.Scanner(System.in);
		java.util.Scanner valor = new java.util.Scanner(System.in);
		int op;
		String c;

		do{
			System.out.println("1 - Busca\n2 - Remove");
			op = a.nextInt();
			switch(op) {
				case 0: 
					break;
				case 1:
					System.out.println("Digite um valor para ser buscado!");
					this.makiEDeLongeAMelhorGarota(valor.nextLine());
					break;
				case 2:
					System.out.println("Digite um valor para ser removido!");
					c = this.exibeBPlus(valor.nextLine());
					this.deletaTudo(c);
					this.printaBplus();
					break;
			}
		}while(op != 0);


	}

	private void deletaTudo(String chaveBusca)
	{
		int i = 0, j = 0;
		BPlus arvore = this.raiz;
		while(!arvore.getEFolha()) arvore = arvore.getPonteiro(0);

		while(arvore != null){
			i = 0;
			while(i < ordem - 1){

				if(chaveBusca.equals(arvore.getIndex(i))){
					if(arvore.length() > this.ordemMinima){
						arvore.deleteIndex(i);
					}else{

					}
				}
				i++;
			}
			j++;
			arvore = arvore.getNodoIrmao();
		}
	}

	private String exibeBPlus(String chaveBusca)
	{
		int i = 0, j = 0;
		BPlus arvore = this.raiz;

		while(!arvore.getEFolha()) arvore = arvore.getPonteiro(0);

		while(arvore != null){
			i = 0;
			while(i < ordem - 1){

				if(chaveBusca.equals(arvore.getIndex(i))){
					return arvore.getIndex(i);
				}
				i++;
			}
			j++;
			arvore = arvore.getNodoIrmao();
		}

		return null;
	}
	private void makiEDeLongeAMelhorGarota(String chaveBusca)
 	{
		int i = 0, j = 0;
		BPlus arvore = this.raiz;
		System.out.println("\nValor pesquisado = " + chaveBusca);
		while(!arvore.getEFolha()) arvore = arvore.getPonteiro(0);
		while(arvore != null){
			i = 0;
			while(i < ordem - 1){

				if(chaveBusca.equals(arvore.getIndex(i))){
					nozomiEAMelhorWaifuSim(arvore,i);
					return;
				}
				i++;
			}
			j++;
			arvore = arvore.getNodoIrmao();
		}




		while(!(arvore.getEFolha())){
			if (arvore.getIndex(i) != null) {
				if ((arvore.getIndex(i).equals(chaveBusca))) {
					arvore = arvore.getPonteiro(i+1);
				} else if (chaveBusca.compareTo(arvore.getIndex(i)) < 0 && i <= 0) {
					arvore = arvore.getPonteiro(i);
					i = 0;
				} else if(chaveBusca.compareTo(arvore.getIndex(i)) < 0 && i > 0) {
					arvore = arvore.getPonteiro(i);
					i = 0;
				} else {
					i++;
				}
			} else {
				arvore = arvore.getPonteiro((arvore.length()-1));
				i = 0;
			}
		}
		i = 0;
		while(i < arvore.length()) {
			if ((arvore.getIndex(i).equals(chaveBusca))) {
				nozomiEAMelhorWaifuSim(arvore,i);
				return;
			}
			i++;
		}
		System.out.println("Valor não encontrado na b+ :(");
 	}
 	private void nozomiEAMelhorWaifuSim(BPlus arvore, int i)
 	{
 		ArrayList<Arquivo> arquivo = arvore.getArquivo(i);
 		if (arquivo == null) {
 			return;
 		}
 		for (int j = 0; j < arquivo.size(); j++) {
 			System.out.println(arquivo.get(j).getValor(atributo));
 		}
 	}
}

package arvorebp.services;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.FileReader;
import arvorebp.model.Arquivo;
import java.util.Collections;

public class Loadcsv
{
	private String path;
	private String key;

	/**
	* path -> Arquivo csv
	* key -> Atributo a ser indexado
	*/
	public Loadcsv(String path, String key)
	{
		this.path = path;
		this.key = key;
	}

	/**
	* Método que tranforma o csv em um vetor de strings
	*
	*/
	public ArrayList<Arquivo> load()
	{
		String line;
		String[] kawaii;
		ArrayList<Arquivo> list = new ArrayList<Arquivo>();

		try{
			// leitura do arquivo
			BufferedReader br = new BufferedReader(new FileReader(this.path));
			// Pega linha por linha e joga em uma célula da string, epois coloca no ArrayList
			while((line = br.readLine()) != null) {
				kawaii = line.split(",", 5);
				list.add(new Arquivo(kawaii));
			}
		} catch(Exception e) {
			System.out.println("Arquivo existe? Erro: " + e);
		}
		// Ordenação dos dados no ArrayList
		int keyOrder = Integer.valueOf(this.key);
		ArquivoComparator comparador = new ArquivoComparator(keyOrder);
		Collections.sort(list, comparador);

		return list;
	}
}

package arvorebp.services;

import arvorebp.model.Arquivo;
import java.util.Comparator;

public class ArquivoComparator implements Comparator<Arquivo>
{
	private int key;

	public ArquivoComparator(int key)
	{
		this.key = key;
	}

	public int compare(Arquivo arq1, Arquivo arq2)
	{
		return arq1.getValor(key).compareTo(arq2.getValor(key));
	}
}